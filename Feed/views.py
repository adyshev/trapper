# Create your views here.
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import View
from Fetch.models import Item


class FeedView(View):
    template_name = 'feed.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, 'Please authenticate first!')
            return redirect('/login/')

        return render(request, self.template_name, {'feed_list': Item.objects.filter(queued=False).order_by('-created')})
