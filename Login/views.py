# Create your views here.
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.views.generic import View


class LoginView(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('login')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            messages.success(request, 'Welcome, my Lord!')
            return redirect('/feed/')
        else:
            messages.warning(request, 'Invalid credentials were given.')

        return render(request, self.template_name)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, 'Please authenticate first!')
        else:
            messages.success(request, 'Goodbye, my Lord!')
            logout(request)

        return redirect('/login/')
