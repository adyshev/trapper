# registry.heet.me/trapper:latest
FROM python:3.8-slim-bullseye
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt-get update -y
RUN apt-get install -y librdkafka-dev libpq-dev gcc python3-dev musl-dev

RUN mkdir /app

RUN pip install --upgrade pip

# Re-use Docker cache
ADD requirements.txt /app/

RUN pip install -r /app/requirements.txt

COPY . /app
