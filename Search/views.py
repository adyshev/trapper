from clutch.schema.user.method.torrent.add import TorrentAddByFilenameArguments
import requests
from bs4 import BeautifulSoup
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.generic import View
from clutch import Client


class SearchView(View):
    template_name = "search.html"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, "Please authenticate first!")
            return redirect("/login/")

        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, "Please authenticate first!")
            return redirect("/login/")

        term = request.POST.get("search", None)
        results = []

        if term is not None and term.strip() != "":
            headers = {
                "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Safari/604.1.38"
            }

            r = requests.get(
                "http://rutor.info/search/0/1/000/0/" + term, headers=headers
            )

            if r.status_code == 200:
                soup = BeautifulSoup(r.text, "html.parser")
                content = soup.find("div", {"id": "content"})
                index = content.find("div", {"id": "index"})

                if index is not None:
                    b_tag = index.find("b").find_all("a")
                    count = list(range(len(b_tag) + 1))

                    for x in count:
                        if x > 0:
                            r = requests.get(
                                "http://rutor.info/search/{0}/1/000/0/".format(x)
                                + term,
                                headers=headers,
                            )
                        results += self.get_items(r)

        return render(request, self.template_name, {"term": term, "result": results})

    def get_items(self, r):
        results = []

        if r.status_code == 200:
            soup = BeautifulSoup(r.text, "html.parser")
            content = soup.find("div", {"id": "content"})
            index = content.find("div", {"id": "index"})
            table = index.find("table")

            for idx, tr in enumerate(table.find_all("tr")):
                if idx > 0:
                    tds = tr.find_all("td")

                    links = tds[1].find_all("a")
                    results.append(
                        {
                            "date": tds[0].contents[0],
                            "name": links[2].contents[0],
                            "magnet": links[0]["href"],
                            "link": "http://rutor.info" + links[2]["href"],
                            "size": tds[3].contents[0]
                            if len(tds) == 5
                            else tds[2].contents[0],
                        }
                    )

        return results


class SearchFetchView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, "Please authenticate first!")
            return redirect("/login/")

        magnet = request.POST.get("magnet", None)

        if magnet is not None:
            client = Client(address="http://transmission-service:9091/transmission/rpc")

            try:
                argument = TorrentAddByFilenameArguments(filename=magnet)
                client.torrent.add(arguments=argument)
                return JsonResponse({"response": "ok"})
            except Exception:
                pass

        return JsonResponse({"response": "nok"})
