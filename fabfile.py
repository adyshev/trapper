import os
from contextlib import contextmanager
from fabric.api import cd, env, prefix, run, sudo, task
from fabric.context_managers import warn_only

PROJECT_NAME = 'Trapper'
PROJECT_ROOT = '/home/pi/trapper'
VENV_DIR = os.path.join(PROJECT_ROOT, 'env')
REPO = 'git@bitbucket.org:adyshev/trapper.git'

env.hosts = ['pi@pi.adyshev.com']


@contextmanager
def source_virtualenv():
    with prefix('source ' + os.path.join(VENV_DIR, 'bin/activate')):
        yield


@task
def deploy():
    """
    Deploys the latest tag to the production server
    """
    # sudo('chown -R %s:%s %s' % (env.user, env.user, PROJECT_ROOT))

    with cd(PROJECT_ROOT):
        run('git pull origin master')
        # with source_virtualenv():
        #     with prefix('export PYTHONIOENCODING=utf-8'):
        #         # run('pip install -r ./requirements.txt')
        #         run('rm -rf static/covers')
        #         run('rm -rf ./db.sqlite3')
        #         run('python ./manage.py migrate')
        #         run('python ./manage.py loaddata feed.json user.json')
        #         run('python ./manage.py fetch_items')
        #         run('python ./manage.py process_queue')


@task
def fetch():
    with cd(PROJECT_ROOT):
        with source_virtualenv():
            run('python ./manage.py fetch_items')
            run('python ./manage.py process_queue')


@task
def bootstrap():
    sudo('rm -rf {}'.format(PROJECT_ROOT))
    sudo('mkdir -p {}'.format(PROJECT_ROOT))
    sudo('chown -R {}:{} {}'.format(env.user, env.user, PROJECT_ROOT))

    with warn_only():
        run('crontab -r')

    run('git clone {} {}'.format(REPO, PROJECT_ROOT))

    with cd(PROJECT_ROOT):
        run('git checkout master')
        run('virtualenv -p python3 env')

        with source_virtualenv():
            with prefix('export PYTHONIOENCODING=utf-8'):
                run('pip install -r ./requirements.txt')
                run('python ./manage.py migrate')
                run('python ./manage.py loaddata feed.json user.json')
                run('python ./manage.py fetch_items')
                run('python ./manage.py process_queue')

                with warn_only():
                    run('circusctl quit')

                run('circusd --daemon ./circus.ini')
                run('crontab ./crontab')
