#!/bin/sh
cd /app
python manage.py migrate
python manage.py loaddata Fetch/fixtures/feed.json
python manage.py loaddata Fetch/fixtures/user.json

/app/bin/fetch.sh

/usr/local/bin/gunicorn -w 4 -b 0.0.0.0:8888 Trapper.wsgi:application --reload
