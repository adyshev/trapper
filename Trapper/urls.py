"""Trapper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from Feed.views import FeedView
from Fetch.views import FetchView
from Login.views import LoginView, LogoutView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from Search.views import SearchView, SearchFetchView

urlpatterns = [
    url(r'^$', FeedView.as_view()),
    url(r'^feed/$', FeedView.as_view(), name="feed"),
    url(r'^login/$', LoginView.as_view()),
    url(r'^search/', SearchView.as_view()),
    url(r'^search_fetch/', SearchFetchView.as_view()),
    url(r'^logout/$', LogoutView.as_view()),
    url(r'^fetch/(?P<item_id>\d+)/$', FetchView.as_view()),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += staticfiles_urlpatterns()
