# Create your views here.
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import View
from Fetch.models import Item
from django.contrib import messages


class FetchView(View):

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.warning(request, 'Please authenticate first!')
            return redirect('/login/')

        item = get_object_or_404(Item, pk=kwargs.get('item_id'), queued=False)
        item.queued = True
        item.save()
        messages.success(request, 'Item "{0}" was added to the queue for downloading.'.format(item.title))

        return redirect('feed')

