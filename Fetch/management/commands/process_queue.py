from clutch.schema.user.method.torrent.add import TorrentAddByFilenameArguments
from django.core.management.base import BaseCommand
from Fetch.models import Item
from clutch import Client


class Command(BaseCommand):
    def handle(self, *args, **options):
        client = Client(address="http://transmission-service:9091/transmission/rpc")

        for item in Item.objects.filter(queued=True, executed=False):
            try:
                argument = TorrentAddByFilenameArguments(filename=item.magnet)
                client.torrent.add(arguments=argument)
                item.executed = True
                item.save()
            except Exception:
                pass
