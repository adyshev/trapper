import hashlib
from time import sleep

import feedparser
import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from bs4 import BeautifulSoup
import errno
import os

from requests import RequestException
from urllib3.exceptions import HTTPError

from Fetch.models import Feed, Item
from PIL import Image


class Command(BaseCommand):
    def handle(self, *args, **options):
        headers = {
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Safari/604.1.38"
        }

        for feed in Feed.objects.all():

            try:
                response = requests.get(feed.url, headers=headers)

                if response.status_code == 200:
                    feed_response = feedparser.parse(response.text)

                    for entry in reversed(feed_response["entries"]):
                        if entry and "link" in entry.keys() and entry.link:
                            hash_key = hashlib.sha224(entry.link.encode()).hexdigest()

                            if not Item.objects.filter(key=hash_key).count():
                                try:
                                    magnet = None
                                    size = None
                                    saved_images = {}
                                    sizes = {}
                                    cover_img = None

                                    response = requests.get(entry.link, headers=headers)

                                    if response.status_code == 200:
                                        soup = BeautifulSoup(
                                            response.text, "html.parser"
                                        )
                                        download_div = soup.find(id="download")

                                        if download_div:
                                            links = download_div.find_all("a")

                                            if len(links) > 2:
                                                magnet = links[1]["href"]

                                        details = soup.find(id="details")

                                        if details:
                                            [
                                                x.extract()
                                                for x in details.findAll("a")
                                                if x is not None
                                            ]

                                            for image in soup.findAll("img"):
                                                if (
                                                    "kinopoisk.ru" not in image["src"]
                                                    and not image["src"].endswith(
                                                        ".gif"
                                                    )
                                                    and "http://s.rutor.info"
                                                    not in image["src"]
                                                ):
                                                    path = os.path.join(
                                                        settings.STATICFILES_DIRS[0],
                                                        "covers",
                                                    )
                                                    file_name = os.path.basename(
                                                        image["src"]
                                                    )
                                                    file_path = os.path.join(
                                                        path, file_name
                                                    )

                                                    if not os.path.isfile(file_path):
                                                        try:
                                                            r = requests.get(
                                                                image["src"],
                                                                headers=headers,
                                                                stream=True,
                                                            )

                                                            if r.status_code == 200:
                                                                self._mkdir_p(path)

                                                                with open(
                                                                    file_path, "wb"
                                                                ) as f:
                                                                    for (
                                                                        chunk
                                                                    ) in r.iter_content(
                                                                        1024
                                                                    ):
                                                                        f.write(chunk)

                                                        except (
                                                            RequestException,
                                                            ConnectionError,
                                                            HTTPError,
                                                            ValueError,
                                                        ):
                                                            pass

                                                    try:
                                                        if os.path.isfile(file_path):
                                                            im = Image.open(file_path)
                                                            w, h = im.size
                                                            img_src = (
                                                                "/static/covers/"
                                                                + file_name
                                                            )
                                                            saved_images[img_src] = (
                                                                w * h
                                                            )
                                                            sizes[img_src] = (w, h)
                                                    except OSError:
                                                        pass

                                            if len(saved_images) > 0:
                                                cover_img = max(
                                                    saved_images, key=saved_images.get
                                                )

                                                for (
                                                    image_path,
                                                    image_size,
                                                ) in saved_images.items():
                                                    if image_path != cover_img:
                                                        path = os.path.join(
                                                            os.path.join(
                                                                settings.STATICFILES_DIRS[
                                                                    0
                                                                ],
                                                                "..",
                                                            ),
                                                            image_path,
                                                        )
                                                        try:
                                                            os.remove(path)
                                                        except OSError:
                                                            pass

                                                wight, height = sizes[cover_img]
                                                if (
                                                    saved_images[cover_img] <= 120000
                                                    or wight > height
                                                ):
                                                    path = os.path.join(
                                                        os.path.join(
                                                            settings.STATICFILES_DIRS[
                                                                0
                                                            ],
                                                            "..",
                                                        ),
                                                        cover_img,
                                                    )
                                                    try:
                                                        os.remove(path)
                                                    except OSError:
                                                        pass
                                                    finally:
                                                        cover_img = None

                                            size = details.find(text=u"Размер")
                                            if size:
                                                td_tag = size.parent
                                                if td_tag:
                                                    size_td = td_tag.findNext("td")
                                                    if size_td:
                                                        size = size_td.getText()

                                    if magnet:
                                        item = Item()
                                        item.key = hash_key
                                        item.size = size
                                        item.magnet = magnet
                                        item.image = (
                                            cover_img
                                            if cover_img is not None
                                            else "/static/not_found.jpg"
                                        )
                                        item.title = entry.title
                                        item.url = entry.link
                                        item.save()

                                except (RequestException, ConnectionError, HTTPError):
                                    pass

                items_count = Item.objects.count()

                if items_count > 100:
                    to_delete = items_count - 100
                    for item in Item.objects.all().order_by("created")[:to_delete]:
                        if item.image != "/static/not_found.jpg":
                            path = os.path.join(
                                os.path.join(settings.STATICFILES_DIRS[0], ".."),
                                item.image,
                            )
                            try:
                                os.remove(path)
                            except OSError:
                                pass

                        item.delete()

            except (RequestException, ConnectionError, HTTPError):
                pass

            sleep(3)

    def _mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise exc
