from django.db import models
from django.utils import timezone


class Feed(models.Model):
    name = models.CharField(max_length=254, blank=False)
    url = models.URLField(unique=True)
    created = models.DateTimeField(default=timezone.now)


class Item(models.Model):
    key = models.CharField(max_length=254, db_index=True)
    size = models.CharField(max_length=128, blank=True, null=True)
    magnet = models.CharField(blank=False, max_length=2048)
    title = models.CharField(max_length=1024, blank=False)
    url = models.URLField(blank=False)
    image = models.CharField(max_length=1024, blank=True, null=True)
    queued = models.BooleanField(default=False)
    executed = models.BooleanField(default=False)
    created = models.DateTimeField(default=timezone.now)

