from django.contrib import admin
from .models import Feed, Item


class FeedAdmin(admin.ModelAdmin):
    list_display = ['name', 'created', 'url', 'created']


class ItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'image', 'url', 'size', 'queued', 'executed', 'created']

admin.site.register(Feed, FeedAdmin)
admin.site.register(Item, ItemAdmin)
